import { Router } from 'express';

import DoctorController from './app/controllers/DoctorController';
import SpecialtyController from './app/controllers/SpecialtyController';

const routes = new Router();

routes.get('/doctors', DoctorController.index);
routes.post('/doctors', DoctorController.store);
routes.put('/doctors/:id', DoctorController.update);
routes.delete('/doctors/:id', DoctorController.delete);

routes.get('/specialties', SpecialtyController.index);
routes.post('/specialties', SpecialtyController.store);
routes.put('/specialties/:id', SpecialtyController.update);
routes.delete('/specialties/:id', SpecialtyController.delete);

export default routes;
