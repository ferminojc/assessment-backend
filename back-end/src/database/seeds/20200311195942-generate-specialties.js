module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('specialties', [
      {
        name: 'ALERGOLOGIA',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'ANGIOLOGIA',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'BUCO MAXILO',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CARDIOLOGIA CLÍNICA',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CARDIOLOGIA INFANTIL',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CIRURGIA CABEÇA E PESCOÇO',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CIRURGIA CARDÍACA',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CIRURGIA DE TORAX',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CIRURGIA GERAL',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CIRURGIA PEDIÁTRICA',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CIRURGIA PLÁSTICA',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CIRURGIA TORÁCIC',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CIRURGIA VASCULAR',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'CLÍNICA MÉDICA',
        created_at: new Date(),
        updated_at: new Date(),
      },
    ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('specialties', null, {});
  },
};
