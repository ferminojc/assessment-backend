import Sequelize, { Model } from 'sequelize';

class Doctor extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        crm: Sequelize.STRING,
        phone: Sequelize.STRING,
        city: Sequelize.TEXT,
        state: Sequelize.TEXT,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsToMany(models.Specialty, {
      through: 'doctor_specialties',
      as: 'specialties',
      foreignKey: 'doctor_id',
    });
  }
}

export default Doctor;
