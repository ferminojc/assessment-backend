import Specialty from '../models/Specialty';

class SpecialtyController {
  async index(req, res) {
    const specialties = await Specialty.findAll();

    return res.json(specialties);
  }

  async store(req, res) {
    const categoryExists = await Specialty.findOne({
      where: { name: req.body.name },
    });

    if (categoryExists) {
      return res.status(400).json({ error: 'Specialty already exists' });
    }

    const category = await Specialty.create(req.body);

    return res.json(category);
  }

  async update(req, res) {
    let category = await Specialty.findByPk(req.params.id);

    if (!category) {
      return res.status(400).json({ error: 'Specialty does not exists' });
    }

    category = await category.update(req.body);

    return res.json(category);
  }

  async delete(req, res) {
    const category = await Specialty.findByPk(req.params.id);

    if (!category) {
      return res.status(400).json({ error: 'Specialty does not exists' });
    }

    category.destroy();

    return res.json({ ok: true });
  }
}

export default new SpecialtyController();
