import Doctor from '../models/Doctor';
import Specialty from '../models/Specialty';

class DoctorController {
  async index(req, res) {
    const doctors = await Doctor.findAll({
      attributes: ['id', 'name', 'crm', 'phone', 'city', 'state'],
      include: [
        {
          model: Specialty,
          as: 'specialties',
          through: { attributes: [] },
        },
      ],
    });

    return res.json(doctors);
  }

  async store(req, res) {
    const doctorExists = await Doctor.findOne({
      where: { name: req.body.name },
    });

    if (doctorExists) {
      return res.status(400).json({ error: 'Doctor already exists' });
    }

    const { specialties, ...data } = req.body;

    if (!(specialties && specialties.length > 0)) {
      return res.status(400).json({ error: 'A Doctor need a specialty' });
    }

    if (specialties.length <= 1) {
      return res
        .status(400)
        .json({ error: 'Doctor need at least two specialties' });
    }

    const doctor = await Doctor.create(data);
    doctor.setSpecialties(specialties);

    return res.json(doctor);
  }

  async update(req, res) {
    let doctor = await Doctor.findByPk(req.params.id);

    if (!doctor) {
      return res.status(400).json({ error: 'Doctor does not exists' });
    }
    const { specialties, ...data } = req.body;

    if (specialties.length <= 1) {
      return res
        .status(400)
        .json({ error: 'Doctor need at least two specialties' });
    }

    doctor = await doctor.update(data);
    doctor.setSpecialties(specialties);

    return res.json(doctor);
  }

  async delete(req, res) {
    const doctor = await Doctor.findByPk(req.params.id);

    if (!doctor) {
      return res.status(400).json({ error: 'Doctor does not exists' });
    }

    doctor.destroy();

    return res.json({ ok: true });
  }
}

export default new DoctorController();
