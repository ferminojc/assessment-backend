module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: 'docker',
  database: 'gcb',
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true,
  },
};
