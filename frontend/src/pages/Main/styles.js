import styled from 'styled-components';

export const Container = styled.div`
  max-width: 700px;
  background: #FFF;
  border-radius: 4px;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.3);
  padding: 30px;
  margin: 80px auto;

  text-align: center;
`;

export const List = styled.ul`
  padding-top: 30px;
  margin-top: 30px;
  list-style: none;

  li {
    padding: 15px 10px;
    border-radius: 4px;
  }
`;
