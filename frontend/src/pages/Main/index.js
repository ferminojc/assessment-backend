import React, { useState, useEffect } from 'react';

import { Container, List } from './styles';

import api from '../../services/api';

export default function Main() {
  const [doctors, setDoctors] = useState([]);

  useEffect(() => {
    async function loadDoctors() {
      const response = await api.get('/doctors');
      setDoctors(response.data);
    }

    loadDoctors();
  });

  return (
    <>
      <Container>
        <h1>Lista de médicos</h1>
        <List>
          {doctors.map(doctor => (
            <>
              <li key={doctor.id}>
                <strong>
                  <h2>Nome - CRM / Especialidade</h2>
                  {doctor.name} - {doctor.crm}
                </strong>
                {doctor.specialties.map(specialty => (
                  <>
                    <li key={specialty.id}>
                      <strong>
                        {specialty.name}
                      </strong>
                    </li>
                  </>
                ))}
              </li>
            </>
          ))}
        </List>
      </Container>
    </>
  );
}
